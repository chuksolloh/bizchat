var pubnub = null;
var me = null;
var Users = null;

var channel = 'memewarz-lobby-demo-20';

var $online_users = $('#online-users');
var $input = $('#chat-input');
var $output = $('#chat-output');

var randomName = function() {

  var animals = ['pigeon', 'seagull', 'bat', 'owl', 'sparrows', 'robin', 'bluebird', 'cardinal', 'hawk', 'fish', 'shrimp', 'frog', 'whale', 'shark', 'eel', 'seal', 'lobster', 'octopus', 'mole', 'shrew', 'rabbit', 'chipmunk', 'armadillo', 'dog', 'cat', 'lynx', 'mouse', 'lion', 'moose', 'horse', 'deer', 'raccoon', 'zebra', 'goat', 'cow', 'pig', 'tiger', 'wolf', 'pony', 'antelope', 'buffalo', 'camel', 'donkey', 'elk', 'fox', 'monkey', 'gazelle', 'impala', 'jaguar', 'leopard', 'lemur', 'yak', 'elephant', 'giraffe', 'hippopotamus', 'rhinoceros', 'grizzlybear'];

  var colors = ['silver', 'gray', 'black', 'red', 'maroon', 'olive', 'lime', 'green', 'teal', 'blue', 'navy', 'fuchsia', 'purple'];

  return colors[Math.floor(Math.random() * colors.length)] + '_' + animals[Math.floor(Math.random() * animals.length)];

};

var randomSkill = function() {
  return Math.floor(Math.random() * 3) + 1;
};

var User_factory = function() {

  var user_list = {};
  var self = this;

  self.remove = function(uuid) {
    delete user_list[uuid];
  };

  self.get = function(uuid) {
    if(user_list.hasOwnProperty(uuid)) {
      return user_list[uuid];
    } else {
      console.error('Trying to retrieve user that is not present.');
    }
  };

  self.set = function(uuid, data) {
    if(!user_list.hasOwnProperty(uuid)) {
      user_list[uuid] = new User(uuid, data);
    }
    return user_list[uuid];
  };

  self.all = function() {
    return user_list;
  }

};

var User = function(uuid, state) {

  var self = this;

  self.uuid = uuid || randomName();
  self.state = state || {skill: randomSkill()};

  var $tpl = $('\
    <li id="' + self.uuid + '" class="list-group-item"> \
    <span class="badge">' + self.state.skill + '</span> \
    ' + self.uuid + ' \
    </li>');

  self.chat = function(text) {

    var $line = $('<li class="list-group-item"><strong>' + self.uuid + ':</strong> </span>');
    var $message = $('<span class="text" />').text(text).html();

    $line.append($message);
    $output.append($line);

    $output.scrollTop($output[0].scrollHeight);

  };

  self.leave = function() {
    $tpl.remove();
  };

  self.init = function() {

    $tpl.click(function() {

      pubnub.publish({
        channel: channel,
        message: {
          type: 'challenge',
          payload: {
            action: 'request',
            uuid: me.uuid,
            target: self.uuid
          }
        }
      });

      alert('Challenging ' + self.uuid + '...');

    });

    $('#online-users').append($tpl);

  };

  return self;

};

var Client = function() {

  var self = new User(randomName());

  self.onRequest = function(caller) {

    var response = confirm(caller.uuid + ' is challenging you to a match! Press OK to accept or Cancel to deny.');

    pubnub.publish({
      channel: channel,
      message: {
        type: 'challenge',
        payload: {
          action: 'response',
          accepted: response,
          uuid: self.uuid,
          target: caller.uuid
        }
      },
      callback: function(){
        alert('Your response has been sent.');
      }
    });
  };

  self.onResponse = function(caller, accepted) {
    if(accepted) {
      alert(caller.uuid + ' has accepted your challenge!');
    } else {
      alert(caller.uuid + ' has rejected your challenge!');
    }
  };

  Users.set(self.uuid, self.state);

  return self;

};

var App = function() {

  Users = new User_factory();
  me = new Client();

  pubnub = PUBNUB.init({
    publish_key: 'demo',
    subscribe_key: 'demo',
    uuid: me.uuid
  });

  pubnub.subscribe({
    channel: channel,
    state: me.state,
    message: function(data) {

      if(data.type == 'chat') {    
        Users.get(data.payload.uuid).chat(data.payload.text);
      }

      if(data.type == 'challenge' && data.payload.target == me.uuid) {

        var challenger = Users.get(data.payload.uuid);

        if(data.payload.action == 'request') {
          me.onRequest(challenger);
        }

        if(data.payload.action == 'response') {
          me.onResponse(challenger, data.payload.accepted);
        }

      }

    }, 
    presence: function(data) {

      if(data.action == "join") {
        Users.set(data.uuid, data.state).init();
      }

      if(data.action == "leave" || data.action == "timeout") {
        Users.remove(data.uuid);
      }

    }
  });

  $('#find-match').click(function(){

    var matches = [];
    var users = Users.all();

    for(var uuid in users) {
      if(uuid !== me.uuid && users[uuid].state.skill == me.state.skill) {
        matches.push(users[uuid]);
      }
    }

    if(!matches.length) {
      alert('Nobody is online with the same skill level.');
    } else {
      var opponent = matches[Math.floor(Math.random() * matches.length)];
      alert('Opponent Found: ' + opponent.uuid + ' with skill level ' + opponent.state.skill);
    }

  });

  $('#chat').submit(function() {

    pubnub.publish({
      channel: channel,
      message: {
        type: 'chat',
        payload: {
          text: $input.val(),
          uuid: me.uuid
        }
      }
    });

    $input.val('');

    return false;

  });

  $('#whoami').text(me.uuid);
  $('#my_skill').text(me.state.skill);

};

App();