var app = angular.module('BizChat', ['ngMaterial', 'ui.router', 'users', 'pubnub.angular.service', 'ngMdIcons']);

app.config(function( $mdIconProvider, $mdThemingProvider, $stateProvider, $urlRouterProvider){
    
    $urlRouterProvider
        .otherwise('/contacts');
    
    $stateProvider
        .state('contacts', {
            url: '/contacts',
            templateUrl: 'views/partials/contacts.html',
            
        })
        .state('chatroom', {
            url: '/chatroom',
            templateUrl: 'views/partials/chatroom2.html',
//            controller: 'AppCtrl'
        })
        .state('rooms', {
            url: '/rooms',
            templateUrl: 'views/partials/rooms.html'
        })
    
    
    $mdIconProvider
        .defaultIconSet("./assets/svg/avatars.svg", 128)
        .icon("menu"       , "./assets/svg/menu.svg"        , 24)
        .icon("share"      , "./assets/svg/share.svg"       , 24)
        .icon("google_plus", "./assets/svg/google_plus.svg" , 512)
        .icon("hangouts"   , "./assets/svg/hangouts.svg"    , 512)
        .icon("twitter"    , "./assets/svg/twitter.svg"     , 512)
        .icon("phone"      , "./assets/svg/phone.svg"       , 512);
    
        
    $mdThemingProvider.definePalette('bizchat', {
//        '400' : '00ACC1',
        
        '50': 'ffebee',
        '100': 'ffcdd2',
        '200': 'ef9a9a',
        '300': 'e57373',
        '400': '00ACC1',
        '500': '00ACC1',
        '600': 'e53935',
        '700': 'd32f2f',
        '800': 'c62828',
        '900': 'b71c1c',
        'A100': 'ff8a80',
        'A200': 'ff5252',
        'A400': 'ff1744',
        'A700': 'd50000',
        'contrastDefaultColor': 'light',    // whether, by default, text (contrast)
                                            // on this palette should be dark or light
        'contrastDarkColors': ['50', '100', //hues which contrast should be 'dark' by default
         '200', '300', '400', 'A100'],
        'contrastLightColors': undefined    // could also specify this if default was 'dark'
    });

    $mdThemingProvider.theme('default')
        .primaryPalette('teal', {
            'hue-1': '100',
            'hue-2': 'A100',
            'hue-3': 'A400'
        })
        .accentPalette('bizchat', {
            'default': '400'
        })
        .warnPalette('red');
    

});

app.service('userProperties', function($rootScope){
    
    var self = this,
        individual = {},
        client = {},
        occupants = {};
        
    individual.selected = '';
    individual.recent = [];
    individual.notify = [];
    occupants.tenants = [];
    
    self.notifyStatus = false;
    
    self.headers = [
    {
      name:'',
      field:'profile_picture'
    },{
      name: 'First Name', 
      field: 'first_name'
    },{
      name: 'Last Name',
      field: 'last_name'
    },{
        name: 'Email',
        field: 'email'
    },{
      name: 'Role', 
      field: 'role'
    },{
      name: 'Department', 
      field: 'department'
    }
      
  ];
    
  self.groupHeaders = [
      {
        name:'Name',
        field:'name'  
      },{
        name:'Description',
        field:'description'  
      },{
        name:'Access',
        field:'access'  
      }
      
  ];
    
  self.groupContents = [
      {
          name: 'Customer Service',
          description: 'Capacity building in customer and information management',
          access: 'public'
      },{
          name: 'Marketing',
          description: 'Creating and expanding scope for new markets',
          access: 'private'
      },{
          name: 'Human Resource',
          description: 'Properly managing labour in work organisation',
          access: 'public'
      }
  ]
  
    
  self.setUser = function (user){
      individual.selected = user;
      localStorage['selected'] = JSON.stringify(individual.selected);
  }
  
  
  self.getUser = function(){
      if($.isEmptyObject(individual.selected)  && localStorage.getItem('selected') != null ){
          individual.selected = JSON.parse(localStorage['selected'])
      }
      return individual.selected;
  }
  
  self.setRecent = function (recent){
      console.log(individual.recent.length)
      var recentExists = false;
      
      for(i=0; i<individual.recent.length; i++){
          if(individual.recent.length > 0){
              if(recent.id_user_string == individual.recent[i].id_user_string){
                  recentExists = true
              }
          } 
      }
      
      if(!recentExists){
          individual.recent.push(recent);
          localStorage['recent'] = JSON.stringify(individual.recent);
      }
      
  }
      
  self.getRecent = function (){
      if(individual.recent.length == 0 && localStorage.getItem('recent') != null){
//          localStorage.clear()
          individual.recent = JSON.parse(localStorage['recent']);
          return individual.recent
      }
      console.log(localStorage.getItem('recent'))
      return individual.recent;
  }
  
  self.setClient = function (user){
      client = user
      localStorage['client'] = JSON.stringify(user)
  }
  
  self.getClient = function (){
      if($.isEmptyObject(client) && localStorage.getItem('recent') != null){
         client = JSON.parse(localStorage['client'])
      }
      return client;
  }
  
  self.setNotify = function (notify){
      individual.notify.push(notify);
  }
  
  self.getNotify = function (){
      return individual.notify;
  }
  
  self.getTenants = function (){
      return occupants.tenants;
  }
  
  self.setTenants = function (tenant){
      var tenantExists = false;
            
      for(i=0; i<occupants.tenants.length; i++){
          if(occupants.tenants.length > 0){
              if(tenant == occupants.tenants[i]){
                  tenantExists = true
              }
          } 
      }
      if(!tenantExists){
          occupants.tenants.push(tenant);
      }
  }
  
  self.showBrowserNotification = function(sender, message){
      var ms = 30000;
           
      if(self.notifyStatus){
//          var notification = new Notification('hi there');
          var notification = new Notification('From '+sender, {
              body: message
          })
          
          notification.onshow = function (){
              setTimeout(notification.close, ms)
          }
//          
//          notification.onclick = function (){
//              window.location.href('http://localhost/bizchat/app/index.html#/chatroom')
//          }
          
      }
      console.log(self.notifyStatus);
  }
  
  self.browserNotification = function(){
     
      if(window.Notification){
          Notification.requestPermission(function(){
              if(Notification.permission === 'granted'){
                  self.notifyStatus = true;
              }
          })
      }
  }
  
  self.checkWindowVisibility = function(sender, message){
      var hidden, visibilityChange;
      
      if(typeof document.hidden !== undefined){
          hidden = "hidden";
          visibilityChange = "visibilityChange"
      }else
      if(typeof document.mozHidden !== undefined){
          hidden = "mozHidden";
          visiblityChange = "mozvisibilitychange";
      }else if(typeof document.msHidden !== undefined){
          hidden = "msHidden";
          visibilityChange = "msvisibilitychange"
      }else if(typeof document.hidden !== undefined){
          hidden = "webkitHidden";
          visibilityChange = "webkitvisibilitychange"
      }
      if(document["hidden"]){
          self.showBrowserNotification(sender, message)
      }
      console.log(hidden)
  }
    
})


app.factory('talentBaseAPI', function($http, $q, $timeout){
    
    function talentBaseAPI(){
        
        var self = this;
        var credentials = {
            'App-key': 'talentbase.bizchat',
            token: '2-n9IR0wuRAyfkI7AIjjSX65fMQXVjdwNP0Nx3BUucHdHcS88a'
        }


        self.staff = null;

        self.getStaffs = function(){
            var deffered = $q.defer()

            if(self.staff !== null){
                deffered.resolve(self.staff + " From cache!")
            }else{
                
                setTimeout(
                    function(){
                        $http.get('http://abclimited.testhired.com.ng/api/v1/staff', { headers: credentials })
                            .then(function(res){
                                    self.staff = res
                                    deffered.resolve(res);
                                },function(res){
                                    deffered.reject(res)
                                }
                             )
                    }, 7000
                )
                
                return deffered.promise;
            }
        }
        
    }
    
    
    return new talentBaseAPI()
    
})


app.controller('AppCtrl', ['$scope', 'userProperties', '$mdDialog', '$mdMedia', 'talentBaseAPI', '$state',  function($scope, userProperties, $mdDialog, $mdMedia, talentBaseAPI, $state){
  
  var self = this,
      log = [];
    
    $scope.success= false;
    $scope.sampUser = userProperties.getClient();
    $scope.recentUserSelected = '';
       
    $scope.staff = null;
    
    $scope.people = function() {
        talentBaseAPI.getStaffs().then(
            function(res){
                angular.forEach(res.data, function(value, key){
                    $scope.content = value
                })
                $scope.success = true;
            },function(error){
                console.log(error);
        }); 
    };
    
    $scope.people();
    
//    userProperties.browserNotification();
    
  $scope.toggleSearch = false;
  
  $scope.status = ' ';
  $scope.recents = userProperties.getRecent();
  $scope.selected = userProperties.getUser();
  $scope.presence = userProperties.getTenants();
  
  
  $scope.$watchCollection('presence', function(newValue, oldValue){
      $scope.presence = newValue;
      $scope.checkPresence = function(selected){
          for(var key in $scope.presence){
              if($scope.presence[key] == selected.id_user_string){
                  return true;
              }
          }
          
      }
  })
  
  
  $scope.$watchCollection('recents', function(newValue, oldValue){
      var recent = newValue;
      $scope.recents = recent;
  })
    
  $scope.groupHeaders = userProperties.groupHeaders;
  $scope.groupContents = userProperties.groupContents;
  $scope.headers = userProperties.headers;
//  $scope.content = userProperties.content;
  if($scope.success == true){
      self.selected = $scope.content[2]
      console.log('hi');
  }
    
  $scope.selectUser = function(user){
      userProperties.setUser(user)
      $scope.selected = userProperties.getUser();
      $state.reload('chatroom');
  }
  
  $scope.selectClient = function(user){
      userProperties.setClient(user);
      $scope.sampUser = userProperties.getClient();
  }
  
  $scope.removeNotifier = function(){
      if($('#notifyRecentUser').hasClass('msg-notifier')){
          $('#notifyRecentUser').removeClass('msg-notifier');
          $('.recent-list-name').css('color', '#9a9a9a')
      }
      
  }
  
  $scope.notify = userProperties.getNotify()
  
  $scope.$watchCollection('notify', function(newValue, oldValue){
      $scope.notify = newValue;
      if(newValue.length > 0){
          for(var key in newValue){
              
              if($scope.sampUser.first_name == newValue[key].receiver){
                  for(var person in $scope.content){
                      if($scope.content[person].first_name == newValue[key].receiver){
                          var data = newValue[key]
                          
                          userProperties.checkWindowVisibility(data.payload.me, data.payload.text)
//                          userProperties.showBrowserNotification(data.payload.me, data.payload.text)
                          $('#notifyRecentUser').addClass('msg-notifier');
                          $('.recent-list-name').css('color', '#fff')
                      }
                  }

              }
              
          }
      }
      console.log(userProperties.getNotify())
  })
 
  
  $scope.createRoom = function(ev){
      
         var useFullScreen = ($mdMedia('sm') || $mdMedia('xs'))  && $scope.customFullscreen;
            $mdDialog.show({
//              controller: DialogController,
              templateUrl: 'views/dialog_creategroup.html',
              parent: angular.element(document.body),
              targetEvent: ev,
              clickOutsideToClose:true,
              fullscreen: useFullScreen
            })
            .then(function(answer) {
              $scope.status = 'You said the information was "' + answer + '".';
            }, function() {
              $scope.status = 'You cancelled the dialog.';
            });
            $scope.$watch(function() {
              return $mdMedia('xs') || $mdMedia('sm');
            }, function(wantsFullScreen) {
              $scope.customFullscreen = (wantsFullScreen === true);
            });
          };
  
    
  $scope.custom = {name: 'bold', description:'grey',last_modified: 'grey'};
  $scope.sortable = ['name', 'email', 'first_name', 'last_name', 'status', 'role', 'department', 'location', 'company'];
  $scope.groupSortable = ['name', 'description', 'access'];

  $scope.thumbs = 'profile_picture';
  $scope.count = 10;
    
    $scope.online = function(status){
        if(status == 'online'){
            $scope.content = userProperties.getTenants();
            console.log(userProperties.getTenants())
        }
        if(status == 'all'){
//            $scope.people();
        }
    }
}]);


app.filter('startFrom',function (){
  return function (input,start) {
    start = +start;
   return input.slice(start);
  }
});

app.directive('scrollBottom', function(){
    return{
        scope:{
            scrollBottom: "="
        },
        link: function($scope, element){
            $scope.$watchCollection('scrollBottom', function(newValue){
                if(newValue){
                    $(element).scrollTop(0);
                    console.log('scrolls well');
                }
            })
        }
    }
        
})

app.directive('currentUser', function (){
    return{
        restrict: 'A',
        scope: {
            
        },
        controller: function($scope){
            $scope.selectNewUser = function (user){
                $scope.selected = userProperties.setUser(user);
                userProper
            }
        }
    }
})

app.directive('mdTable', function () {
  return {
    restrict: 'E',
    scope: { 
      headers: '=head',
      content: '=data', 
      sortable: '=sort', 
      filters: '=',
      customClass: '=customClass',
      thumbs:'=', 
      count: '='
    },
    controller: function ($scope,$filter,$window,$state,userProperties) {
      var orderBy = $filter('orderBy');
      $scope.tablePage = 0;
      $scope.nbOfPages = function () {
        return Math.ceil($scope.content.length / $scope.count);
      };
      	$scope.handleSort = function (field) {
          if ($scope.sortable.indexOf(field) > -1) { return true; } else { return false; }
                    
      };
      $scope.order = function(predicate, reverse) {
          $scope.content = orderBy($scope.content, predicate, reverse); 
          $scope.predicate = predicate;
      };
      $scope.order($scope.sortable[0],false);
      $scope.getNumber = function (num) {
      			    return new Array(num);
      };
      $scope.goToPage = function (page) {
        $scope.tablePage = page;
      };  
      $scope.userListChat = function(user){
          $scope.selected = userProperties.setUser(user)
          userProperties.setRecent(user);
          $state.go('chatroom'); 
      }
      
      
    },
    template: angular.element(document.querySelector('#md-table-template')).html()
  }
});

app.controller('chatCtrl', function($rootScope, $scope, $location, userProperties, PubNub){
    
    var pubnub = null,
        element = document.getElementsByClassName('bottom-margin')[0];
        $('.bottom-margin').scrollTop($('.bottom-margin')[0].scrollHeight);
            
    $scope.userSelected = userProperties.getUser();
    $scope.client = userProperties.getClient();
    $scope.sub_key = "sub-c-ddcc5c7a-bf8c-11e5-a9aa-02ee2ddab7fe";
    $scope.pub_key = "pub-c-3d7822ec-5af2-48f4-a015-ef74b1000926";
    $scope.userid = $scope.sub_key + $scope.client.id_user_string;
    $scope.channel = 'bizchatchannel';
    $scope.roomName = 'Chat with ' + $scope.userSelected.first_name + ' ' + $scope.userSelected.last_name;
    $scope.messages = [];
    $scope.jsonMessage = []
    $scope.history = [];
    $scope.available = [];
    
    userProperties.browserNotification();
    
    
    $scope.sortChat = function(message){
//        console.log(message.receiver + '=' + $scope.userSelected.first_name)
//        console.log(message.payload.me + '=' + $scope.client.first_name)
//        console.log(message)
        
        if(message.receiver == $scope.userSelected.first_name && message.payload.me == $scope.client.first_name || message.receiver == $scope.client.first_name && message.payload.me == $scope.userSelected.first_name){
            $scope.messages.push(message)
        } 
        
        
    }
    
    $scope.presenceEvent = function(message, announce){
        
        if(announce == undefined) announce = true;
        if(message['action'] == 'join'){
            $scope.available.push(message.uuid)
        }else if(message['action'] == 'leave'){
            console.log('leave')
        }else if(message['action'] == 'timeout'){
            console.log('timeout');
            pubnub.unsubscribe({
                channel: $scope.channel
            })
        }
    }
    
    pubnub = PUBNUB({
        subscribe_key: $scope.sub_key,
        publish_key: $scope.pub_key,
        uuid: $scope.userid,
        heartbeat: 120,
//        heartbeat_interval: 30
    })
     
    pubnub.subscribe({
        channel: $scope.channel,
        state: {
            channel: $scope.channel,
            uuid: $scope.userid,
            state: $scope.client,
            callback: function(m){
                console.log(m);
            },
            error: function(m){
                console.log(m)
            }
            
        },
        connect: function(){
            pubnub.history({
                channel: $scope.channel,
                limit: 100,
                callback: function(m){
                    var text = m[0];
                    for(var key in text){
                        if(typeof text[key].receiver == 'undefined' || typeof text[key].payload.me == 'undefined'){
                            continue
                        }
                        $scope.$apply(function(){
                            $scope.sortChat(text[key])
                        }) 
                    }
                    $('html, body').animate({scrollTop:$('.bottom-margin').height()}, 2000)
                }
            });
            pubnub.here_now({
                channel: $scope.channel,
                state: true,
                callback: function (message){
                    var state = message.uuids
                    for (var key in state){
                            
                        if(typeof state[key].state != "undefined" && state[key].state.state.hasOwnProperty('id_user_string')){
                            userProperties.setTenants(state[key].state.state.id_user_string);
                        }  
                        
                    }
                    
                }
            })
        },
        message: function(data){
            $scope.$apply(function(){
                $scope.sortChat(data);
                userProperties.setNotify(data);
            })
            $('html, body').animate({scrollTop:$('.bottom-margin').height()}, 2000)
            
        },
        presence: function(message){
            $scope.presenceEvent(message, true)
        },
        
    })
    
    
    $scope.publish = function(){
        
        pubnub.publish({
            
            channel: $scope.channel,
            message: {
                receiver: $scope.userSelected.first_name,
                payload:{
                    "text":$scope.newMessage,
                    "channel":$scope.channel, 
                    "sent-time": Date.now(),
                    "me": $scope.client.first_name
                }
            }
//            callback: function(m){ console.log(m) }
        });
        
        $scope.newMessage = ''
        
    }
    
        
    
    
    
})
