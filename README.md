# BizChat Web

## Getting Started

All you need to do is to clone this repository,


```bash
git clone https://Victoradebola@bitbucket.org/chuksolloh/bizchat.git
cd bizchat
```

Then, install all the dependencies:

```
npm install
bower install
```
